## Setup Instructions

- Add the repo as a Git submodule folder in your project:
```
git submodule add https://bitbucket.org/storeganise/sg-www-server
```

- Do `npm install`

- Copy files from the `_copy-to-root` folder into your project root.

- Customise views, routes, styles etc as required
