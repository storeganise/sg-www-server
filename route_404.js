const wurd = require('wurd');


module.exports = [
  wurd.mw('common,404'),
  function(req, res, next) {
    res.status(404).render('404');
  }
];