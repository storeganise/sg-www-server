/**
 * Returns a full config object
 */
 
const env = process.env || {};


/**
 * @param {String} slug               Operator ID e.g. 'spaceup'; used for generating various config settings
 * @param {Object} [customConfig]     Optional object to override default config settings
 */
module.exports = (slug, customConfig) => {

  const appUrl = customConfig.appUrl || env.APP_URL || `http://${slug}-user.storeganise.com`;

  const defaultConfig = {
    port: env.PORT || 8000,

    forceHttps: (env.FORCE_HTTPS === '1'),

    wurdApp: `sg-${slug}-www`,

    urls: {
      app: appUrl,
      login: appUrl,
      signup: `${appUrl}/#/signup`,
      getStarted: `${appUrl}/#/get-started`,
    },

    //Available languages
    languages: ['en'],

    //Plugins
    googleAnalyticsId: env.GOOGLE_ANALYTICS_ID,
    olarkId: env.OLARK_ID,
  };

  return Object.assign(defaultConfig, customConfig);
};
