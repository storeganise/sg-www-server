/**
 * Default routes
 */
const wurd = require('wurd');


module.exports = (app) => {

  app.get('/', wurd.mw('common,home'), (req, res, next) => {
    res.render('home', {
      locals: res.locals
    });
  });


  app.get('/help', wurd.mw('common,help'), (req, res, next) => {
    res.render('help', {
      locals: res.locals
    });
  });


  app.get('/terms', wurd.mw('common,terms'), (req, res, next) => {
    res.render('text', {
      locals: res.locals,
      pageName: 'terms'
    });
  });

};
