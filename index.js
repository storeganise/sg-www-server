const http = require('http');
const path = require('path');
const express = require('express');
const config = require('../config');

const wurd = require('wurd');


// Create app
let app = module.exports = express();
let server = http.createServer(app);



// ==================================================================================
// CONFIG
// ==================================================================================

// View setup
app.set('views', [
  path.join(__dirname, '../views'),
  path.join(__dirname, 'views'),
]);
app.set('view engine', 'js');
app.engine('js', require('express-react-views').createEngine());

// Force HTTPS
if (config.forceHttps) {
  app.use(require('heroku-utils').forceHttps);
}

// Main middleware stack
app.use(require('less-middleware')(path.join(__dirname, 'public'), {
  dest: path.join(__dirname, '../public')
}));
app.use(require('compression')());
app.use(require('cookie-parser')());

// Static files
app.use(express.static(path.join(__dirname, 'public')));    // Default files
app.use(express.static(path.join(__dirname, '../public'))); // Custom files


// ==================================================================================
// SETUP
// ==================================================================================

// CMS
app.use(wurd.connect(config.wurdApp, {
  editMode: 'querystring',
  blockHelpers: {
    Wurd: require('./views/components/wurd'),
    WurdImage: require('./views/components/wurd-image')
  }
}));


// Multiple languages
app.use(require('express-request-language')({
  languages: config.languages,
  queryName: 'lang',
  cookie: { name: 'lang' }
}));

app.use((req, res, next) => {
  //Set the langauge for this request; it is used by wurd.mw middleware
  req.wurd.lang = req.language;

  //Make language available to templates for languade switcher etc.
  res.locals.currentLanguage = req.language;

  next();
});


// Routes
require('./routes')(app);  // Default routes
require('../routes')(app); // Custom routes

// 404 handler (must come last)
app.use(require('./route_404'));



// ==================================================================================
// RUN
// ==================================================================================

//Run the server if running as main app
if (!module.parent) {
  server.listen(config.port, function() {
    console.log("Express server listening on port %d", server.address().port);
    console.log('Environment: ' + process.env.NODE_ENV);
  });
}
