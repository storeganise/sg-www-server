const React = require('react');
const {urls} = require('../../config');

const {Layout, MainButton} = require('./components');
const Wurd = require('./components/wurd')


module.exports = ({ locals, wurd }) => {

  const content = wurd.block('404');


  return (
    <Layout 
      locals={locals}
      wurd={wurd}
      pageId={content.id()}
      urls={urls}
    >
      <section>
        <h2><Wurd block={content} id="title" /></h2>

        <MainButton
          url="/"
          content={content.block('button')}
        />
      </section>
    </Layout>
  );

};
