/**
 * Footer component
 * To override, copy it into the project's own views/components folder and customise there
 */
const React = require('react');


module.exports = ({content, className, style, showPrivacy = false}) => {

  const {Wurd, text} = content;

  const phoneLink = 'tel:' + (text('phone') || '').replace(/ /g, '');


  return (
    <footer className={className} style={style}>
      <div className="container">
        <div className="row">
          <div className="col-sm-12 text-center">
            <h2><Wurd id="title" /></h2>

            <ul className="list-inline" style={{marginTop: 20}}>
              <li><a href={phoneLink}><i className="fa fa-fw fa-2x fa-phone-square"></i> <Wurd id="phone" /></a></li>
              <li><a target="_blank" href={`mailto:${text('email')}`}><i className="fa fa-fw fa-2x fa-envelope-square"></i> <Wurd id="email" /></a></li>
            </ul>
          </div>
        </div>
        <div className="row" style={{marginTop: 40}}>
          <div className="col-sm-6">
            <ul className="links list-inline container">
              {showPrivacy &&
                <li><a href="/privacy"><Wurd id="privacy" /></a></li>
              }
              <li><a href="/terms"><Wurd id="terms" /></a></li>
            </ul>
          </div>
          <div className="col-sm-6">
            <div className="text-right"><a href="http://storeganise.com" target="_blank">Powered by Storeganise valet storage software</a></div>
          </div>
        </div>
      </div>
    </footer>
  );

};
