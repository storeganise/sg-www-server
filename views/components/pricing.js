const React = require('react');


module.exports = ({urls, content, showPlanButtons = false}) => {

  const {Wurd} = content;


  return (
    <div className="pricing">
      <h2><Wurd id="title" /></h2>

      <Wurd el="div" block={content} id="intro" markdown className="text-center" />
      
      <div data-wurd-list={content.id('items')} data-wurd-list-props="title">
        {content.map('items', ({id, text, Wurd, WurdImage}) => 
          <div key={id()} className="row item">
            <div className="col-sm-4 image">
              <WurdImage id="image" style={{width: 220}} />
            </div>
            <div className="col-sm-5 content">
              <h3><Wurd id="title" /></h3>
              <Wurd el="div" id="text" markdown />
            </div>
            <div className="col-sm-3 text-center">
              <div className="price">
                <Wurd el="div" id="price" markdown />
              </div>

              {showPlanButtons &&
                <span data-wurd-obj={id()} data-wurd-obj-props="plan,button">
                  <a className="btn btn-primary" href={`${urls.getStarted}?plan=${text('plan')}`}>{text('button')}</a>
                </span>
              }
            </div>
          </div>
        )}
      </div>
    </div>
  );

};
