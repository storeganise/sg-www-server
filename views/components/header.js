/**
 * Header component
 * To override, copy it into the project's own views/components folder and customise there
 */
const React = require('react');


module.exports = ({urls, content, className, style}) => {

  const {Wurd, text} = content;

  const loginUrl = text('loginUrl') || urls.login;
  const signupUrl = text('signupUrl') || urls.getStarted;

  
  return (
    <header className={className} style={style}>
      <nav id="nav" className="navbar navbar-fixed-top" role="navigation">
        <div className="container-fluid">
          <div className="navbar-header">

            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="nav .collapse">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>

            <a className="navbar-brand" href="/" data-wurd-obj={content.id()} data-wurd-obj-props="brand">{text('brand')}</a>
          </div>

          <div className="collapse navbar-collapse navbar-right">
            <ul className="nav navbar-nav">
              <li className="hidden-sm navbar-divider"><a className="email" target="_blank" href={`mailto:${text('email')}`}><i className="glyphicon glyphicon-envelope"></i> <Wurd id="email" /></a></li>
              <li className="hidden-sm"><a className="how-it-works-navlink" href="/#how-it-works"><Wurd id="howWorks" /></a></li>
              <li><a className="hidden-sm pricing-navlink" href="/#pricing"><Wurd id="pricing" /></a></li>
              <li><a href="/help"><Wurd id="help" /></a></li>
              
              <li data-wurd-obj={content.id()} data-wurd-obj-props="login,loginUrl">
                <a href={loginUrl}>{text('login')}</a>
              </li>
              <li data-wurd-obj={content.id()} data-wurd-obj-props="signup,signupUrl" className="signup">
                <a href={signupUrl}>{text('signup')}</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );

};
