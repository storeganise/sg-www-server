const React = require('react');


module.exports = ({content}) => {

  const {Wurd} = content;

  return (
    <div 
      className="detailed-features" 
      data-wurd-list={content.id('items')}
      data-wurd-list-props="title,text"
    >
      {content.map('items', ({id, text, Wurd}) => 
        <div 
          key={id()} 
          className="detailed-features__item"
          data-wurd-bgimg={id('image')} 
          style={{backgroundImage: `url(${text('image')})`}}
        >
          <h2><Wurd id="title" /></h2>
          <Wurd el="div" id="text" markdown />
        </div>
      )}
    </div>
  );

};
