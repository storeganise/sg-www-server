const React = require('react');
const {urls} = require('../../../config');


module.exports = ({url, content, style}) => {

  const t = content.text;

  url = url || t('url') || urls.getStarted;


  return (
    <section data-wurd-obj={content.id()} data-wurd-obj-props="text,url" className="main-button" style={style}>
      <a href={url} className="btn btn-primary">{t('text')}</a>
    </section>
  );

};
