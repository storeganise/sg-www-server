module.exports = {
  DetailedFeatures: require('./detailed-features'),
  Footer: require('./footer'),
  Header: require('./header'),
  Hero: require('./hero'),
  HowItWorks: require('./how-it-works'),
  Layout: require('./layout'),
  MainButton: require('./main-button'),
  MetaEditor: require('./meta-editor'),
  OneLineFeatures: require('./one-line-features'),
  Pricing: require('./pricing'),
  PricingSimple: require('./pricing-simple'),
  TextBlock: require('./text-block')
};
