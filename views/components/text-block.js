const React = require('react');


module.exports = ({ content }) => {

  const {Wurd} = content;

  return (
    <Wurd el="div" markdown />
  );

};
