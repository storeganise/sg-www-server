const React = require('react');
const {omit} = require('lodash');

const myProps = [
  'block',
  'id'
];


module.exports = function(props) {

  const block = props.block || this;
  const {id} = props;

  const url = block.text(id);

  const elProps = omit(props, myProps);

  elProps.src = url;
  elProps.alt = elProps.alt || '';

  if (block.editMode) {
    elProps['data-wurd-img'] = block.id(id);
  }

  return React.createElement('img', elProps);

};
