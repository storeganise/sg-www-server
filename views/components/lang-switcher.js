const React = require('react');


module.exports = ({languages, currentLanguage, content}) => {
  if (languages.length <= 1) return null;

  const {Wurd} = content;
  
  const otherLanguage = languages.find(lang => lang !== currentLanguage);

  return (
    <a className="lang-switcher" href={`?lang=${otherLanguage}`}>
      <Wurd id={otherLanguage} />
    </a>
  );

};
