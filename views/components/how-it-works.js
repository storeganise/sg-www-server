const React = require('react');

const helpers = require('../helpers');


module.exports = ({ content }) => {

  const {Wurd} = content;

  // Decide how wide each item should be based on the number to display
  const numItems = Object.keys(content.get('steps') || {}).length;
  const colSize = helpers.getColSize(numItems);


  return (
    <div className="how-it-works container">
      <h2><Wurd id="title" /></h2>

      <ul data-wurd-list={content.id('steps')} data-wurd-list-props="title,text" className="row">
        {content.map('steps', ({id, Wurd, WurdImage}) => 
          <li key={id()} className={`col-sm-${colSize} icon`}>
            <WurdImage id="image" />
            <h3><Wurd id="title" /></h3>
            <p><Wurd id="text" /></p>
          </li>
        )}
      </ul>
    </div>
  );
  
};
