const React = require('react');
const {omit} = require('lodash');

const myProps = [
  'block',
  'id',
  'vars',
  'el',
  'markdown'
];

module.exports = function(props) {
  
  const block = props.block || this;
  const {id, vars, el, markdown} = props;

  const elProps = omit(props, myProps);

  let text;

  if (markdown) {
    elProps.dangerouslySetInnerHTML = {
      __html: block.markdown(id, vars)
    };
  } else {
    text = block.text(id, vars);
  }

  if (block.editMode) {
    const editorType = (vars || markdown) ? 'data-wurd-md' : 'data-wurd';

    elProps[editorType] = block.id(id);

    return React.createElement(el || 'span', elProps, text);
  } else {
    return React.createElement(el || React.Fragment, elProps, text);
  }

};
