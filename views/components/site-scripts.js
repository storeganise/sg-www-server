/**
 * Scripts that are added to all pages
 * To override, copy it into the project's own views/components folder and customise there
 */
const React = require('react');


module.exports = () => {
  return <div dangerouslySetInnerHTML={{__html: ``}} />
};
