const React = require('react');

const helpers = require('../helpers');


module.exports = ({ content }) => {

  const {Wurd} = content;

  // Decide how wide each item should be based on the number to display
  const numItems = Object.keys(content.get('items') || {}).length;
  const colSize = helpers.getColSize(numItems);

  return (
    <div className="pricing-simple container">
      <h2><Wurd id="title" /></h2>
      
      <ul data-wurd-list={content.id('items')} className="row">
        {content.map('items', ({id, Wurd, WurdImage}) => 
          <li key={id()} className={`col-sm-${colSize}`}>
            <div className="image">
              <WurdImage id="image" style={{width: 220}} />
            </div>
            <h3><Wurd id="title" /></h3>
            <Wurd el="div" id="text" markdown />
          </li>
        )}
      </ul>
    </div>
  );

};
