const React = require('react');


module.exports = ({ content }) => {

  const {Wurd, text} = content;


  return (
    <div className="one-line-features">
      <Wurd id="intro" markdown el="div" />

      <ul data-wurd-list={content.id('items')} data-wurd-list-props="text" className="row list-unstyled">
        {content.map('items', ({id, Wurd}) => 
          <Wurd 
            key={id()} 
            el="li" 
            className="col-sm-6" 
            id="text" 
            markdown 
          />
        )}
      </ul>

      <Wurd id="outro" markdown el="div" />
    </div>
  );

};
