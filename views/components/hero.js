const React = require('react');


module.exports = ({urls, content}) => {
  
  const {Wurd, text} = content;

  const buttonUrl = text('buttonUrl') || urls.getStarted;

  return (
    <div 
      className="hero" 
      data-wurd-bgimg={content.id('image')}
      style={{backgroundImage: `url(${text('image')})`}}
    >
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <Wurd el="div" id="title" markdown />

            <a 
              data-wurd-obj={content.id()} 
              data-wurd-obj-props="button,buttonUrl" 
              href={buttonUrl} 
              className="btn btn-large btn-primary"
            >
              {text('button')}
            </a>
          </div>
        </div>
      </div>
    </div>
  );

};
