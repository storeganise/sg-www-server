const React = require('react');
const {urls} = require('../../config');

const {Layout, TextBlock} = require('./components');


module.exports = ({ locals, wurd, pageName }) => {

  const content = wurd.block(pageName);


  return (
    <Layout 
      locals={locals}
      wurd={wurd}
      pageId={content.id()}
      urls={urls}
    >
      <div className="container">
        <TextBlock
          content={content.block('text')}
        />
      </div>
    </Layout>
  );

};
