const React = require('react');
const {urls} = require('../../config');

const {
  Layout,
  MainButton,
  Hero,
  HowItWorks,
  Pricing,
  PricingSimple,
  DetailedFeatures
} = require('./components');


module.exports = ({ locals, wurd }) => {

  const content = wurd.block('home');


  return (
    <Layout 
      locals={locals}
      wurd={wurd}
      pageId={content.id()}
      urls={urls}
    >
      <Hero
        urls={urls}
        content={content.block('hero')}
      />

      <a name="how-it-works"></a>
      <section className="alt-section">
        <HowItWorks
          content={content.block('howWorks')}
        />
      </section>

      <a name="pricing"></a>
      <section>
        <Pricing
          urls={urls}
          content={content.block('plans')}
        />

        <MainButton
          content={content.block('planButton')}
        />

        <PricingSimple
          urls={urls}
          content={content.block('items')}
        />

        <MainButton
          content={content.block('itemsButton')}
        />
      </section>

      <section>
        <DetailedFeatures
          content={content.block('features')}
        />

        <MainButton
          content={content.block('featuresButton')}
        />
      </section>
    </Layout>
  );

};
