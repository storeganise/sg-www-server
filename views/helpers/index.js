exports.getColSize = function(numItems, minColSize = 4) {
  let colSize = Math.round(12 / numItems);

  if ((colSize < minColSize) && (12 % numItems === 0)) {
    colSize = colSize * 2;
  }

  return colSize;
};
