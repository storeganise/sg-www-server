const React = require('react');
const {urls} = require('../../config');

const {
  Layout,
  TextBlock
} = require('./components');

const Wurd = require('./components/wurd');


module.exports = ({ locals, wurd }) => {

  const content = wurd.block('help');


  return (
    <Layout 
      locals={locals}
      wurd={wurd}
      pageId={content.id()}
      urls={urls}
    >
      <div className="container">
        <h1><Wurd block={content} id="title" /></h1>

        <div className="alert alert-info">
          <Wurd el="div" block={content} id="intro" markdown />
        </div>

        <TextBlock
          content={content.block('faq')}
        />
      </div>
    </Layout>
  );

};
